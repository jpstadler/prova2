
import utfpr.ct.dainf.pratica.PontoXZ;
import utfpr.ct.dainf.pratica.PontoXY;
import utfpr.ct.dainf.pratica.PontoYZ;
import utfpr.ct.dainf.pratica.Ponto;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author 
 */
public class Pratica {

    public static void main(String[] args) {
        PontoXZ p1 = new PontoXZ(-3,2);
        PontoXY p2 = new PontoXY(0,2);
        
        PontoXY p3 = new PontoXY();
        
        System.out.println(p1.equals(new String()));
        
        
        System.out.println("Distância = "+p1.dist(p2));
        
    }
    
}
